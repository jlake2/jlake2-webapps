#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'jlake_DB'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to feednd DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################

#Hours
dropTableQuery = ("DROP TABLE IF EXISTS hours")
cursor.execute(dropTableQuery)


#Restaurants
dropTableQuery = ("DROP TABLE IF EXISTS restaurants")
cursor.execute(dropTableQuery)


#Need to drop everything else: 

#menus: 
dropTableQuery = ("DROP TABLE IF EXISTS menus")
cursor.execute(dropTableQuery)

#menuItems: 
dropTableQuery = ("DROP TABLE IF EXISTS menuItems")
cursor.execute(dropTableQuery)

#orders:
dropTableQuery = ("DROP TABLE IF EXISTS orders")
cursor.execute(dropTableQuery)

#orderItems:
dropTableQuery = ("DROP TABLE IF EXISTS orderItems")
cursor.execute(dropTableQuery)


########################
## Create tables next ##
########################


#Restaurants
createTableQuery = ('''CREATE TABLE restaurants (
						restId VARCHAR(20) NOT NULL,
						name VARCHAR(45) NOT NULL,
						address VARCHAR(100) NOT NULL,
						city VARCHAR(45) NOT NULL,
						state VARCHAR(20) NOT NULL,
						zip VARCHAR(10) NOT NULL,
						phone VARCHAR(20) NOT NULL,
						lat DECIMAL(10,8) NOT NULL,
						lng DECIMAL(11,8) NOT NULL,
                                                url VARCHAR(100),
						PRIMARY KEY (restId));'''
                    )
cursor.execute(createTableQuery)

#Hours
createTableQuery = ('''CREATE TABLE hours (
						restId VARCHAR(20) NOT NULL,
                                                day enum ('M','T','W','TH','F','S','SU') not null,
                                                open TIME NOT NULL,
                                                close TIME NOT NULL,
                                                PRIMARY KEY(restId,day,open),
                                                FOREIGN KEY(restId)
                                                REFERENCES restaurants(restId)
                                                ON DELETE CASCADE
                                          );'''
                    )
cursor.execute(createTableQuery)
#Menu holds the different menus for all restaurants
createTableQuery = ('''CREATE TABLE menus (
					restId VARCHAR(20) NOT NULL, 
					menuId VARCHAR(20) NOT NULL,
					restName VARCHAR(45) NOT NULL,
					menuName VARCHAR(45) NOT NULL,
				        PRIMARY KEY(menuId,menuName,restName),
					FOREIGN KEY(restId) REFERENCES restaurants(restId) ON DELETE CASCADE
			);'''
		)
cursor.execute(createTableQuery)


#menuItems holds the different items in all of the menus, along with their prices
createTableQuery = ('''CREATE TABLE menuItems (
					menuId VARCHAR(20) NOT NULL,
					itemId VARCHAR(20) NOT NULL,
					menuName VARCHAR(45) NOT NULL,
					sect NVARCHAR(45) NOT NULL,
					name VARCHAR(45) NOT NULL,
					des VARCHAR(200),
					price FLOAT,
					PRIMARY KEY(itemId,sect,name),
					FOREIGN KEY(menuId) REFERENCES menus(menuId) ON DELETE CASCADE
			);'''
		)
cursor.execute(createTableQuery)



#Orders
createTableQuery = ('''CREATE TABLE orders (
				orderId int NOT NULL AUTO_INCREMENT, 
				nameFirst VARCHAR(20) NOT NULL,
				nameLast VARCHAR(20) NOT NULL,
				PRIMARY KEY(orderID,nameFirst,nameLast)
			);'''
		)
cursor.execute(createTableQuery)
#OrderItems
createTableQuery = ('''CREATE TABLE orderItems (
					orderItemId int NOT NULL AUTO_INCREMENT,
					orderId int NOT NULL, 
					itemId  VARCHAR(20) NOT NULL,		
					itemName VARCHAR (45) NOT NULL,
					numItems VARCHAR (20) NOT NULL,
					PRIMARY KEY(orderItemId),
					FOREIGN KEY(orderId) REFERENCES orders(orderId) ON DELETE CASCADE
			);'''
		)
cursor.execute(createTableQuery)

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()
