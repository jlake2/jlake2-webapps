#John F. Lake, Jr. 
# Handler for /orders/
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
from orderid import OrderID
import logging
from config import conf

class Orders(object):
    exposed = True

    def __init__(self):
	self.id = OrderID()  #points to orderid.py

    def _cp_dispatch(self,vpath):
	print "OrderItems._cp_dispatch with vpath: %s \n" % vpath
	if len(vpath) == 1: # /orders/{orderId}
	    cherrypy.request.params['orderId']=vpath.pop(0)
	    return self.id
	if len(vpath) == 2: # /orders/{orderId}/items/
	    cherrypy.request.params['orderId']=vpath.pop(0)
	    vpath.pop(0) # items
	    return self.id.items
	if len(vpath) == 3: # /orders/{orderId}/items/{itemId}
	    cherrypy.request.params['orderId']=vpath.pop(0)
	    vpath.pop(0) # items
	    cherrypy.request.params['itemId']=vpath.pop(0)
	    return self.id.items.id
	return vpath



    def GET(self):
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='jlake_DB',charset='utf8mb4')        
	cursor = cnx.cursor()
	q = "select * from orders;"
	cursor.execute(q)
	info = list(cursor.fetchall())

        return env.get_template('orders-tmpl.html').render(
                userName="User: John Lake",
                orders=list(info),
                base=cherrypy.request.base.rstrip('/') + '/'
        )

    def POST(self):
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='jlake_DB',charset='utf8mb4')        
	cursor = cnx.cursor()
	q = "insert into orders (nameFirst,nameLast) values (\"John\",\"Lake\") ;"
	cursor.execute(q)
	cnx.commit()
	cnx.close()
        return "You just made a POST call to orders.  An order was created!"

    def OPTIONS(self):
        ''' Allows GET, POST, OPTIONS '''
        #Prepare response
        return "<p>/orders/ allows GET, POST, and OPTIONS</p>"

class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    }
    cherrypy.tree.mount(Orders(), '/orders', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(Orders(), None, conf)

