#John F. Lake, Jr. 
#Handler for /orders/{ID}/items

import apiutil
import cherrypy
import mysql.connector
from mysql.connector import Error
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import json
from apiutil import errorJSON,  SESSION_KEY
from config import conf
from orderItemsid import OrderItemsID

class OrderItems(object):
    exposed = True
    @cherrypy.tools.json_in()
    def __init__(self):
	self.id = OrderItemsID()

    
    def GET(self, orderId):
	return "You made a GET call to /orders/{orderId}/items"

    def DELETE(self, orderId):
	return "You made a DELETE call to /orders/{orderId}/items"

    @cherrypy.tools.json_in(force=False)
    def PUT(self, orderId):
	return "You made a PUT call to /orders/{orderId}/items"

    def OPTIONS(self,orderId):
	return "You can use GET, PUT, DELETE, and OPTIONS on /orders/{orderId}/items"


