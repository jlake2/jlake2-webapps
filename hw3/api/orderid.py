#John F. Lake
#Handles /orders/{id}
import cherrypy
import mysql.connector
from orderItems import OrderItems

class OrderID(object):
    exposed = True

    def __init__(self):
        self.items=OrderItems()

    def GET(self, orderId):
        ''' Return information on order restId'''

        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='jlake_DB',charset='utf8mb4')        
	cursor = cnx.cursor()
	q = "select * from orderItems where orderId='%s';" % orderId
	cursor.execute(q)
	info = list(cursor.fetchall())
	result = "<!DOCTYPE=html><html><body>"
	for item in info:
		l = list(item)
		result = result + "Item: "+ l[3].encode('utf-8') + " Price: "+ str(l[4]) +"<br>"
	result = result + "</body><html>"
	return result  
    def PUT(self, orderId, **kwargs):
        ''' Update order with restId'''
        result = "You called PUT on /orders/{orderId} \n"
        result += "PUT body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update order
        # Prepare response
        return result

    def DELETE(self, orderId):
        #Validate restId
        #Delete order
        #Prepare response
        return "You attempted to DELETE /orders/{id=%s}, but it isn't implemented yet." % orderId

    def OPTIONS(self, orderId):
        ''' Allows GET, PUT, DELETE, OPTIONS '''
        #Prepare response
        return "<p>/orders/{orderId} allows GET, PUT, DELETE, and OPTIONS</p>"
