#Use this script to populate the |restaurant| table of the database

import mysql.connector
import hashlib
import json
from decimal import *
import random
import sys
reload(sys)
sys.setdefaultencoding("utf8")

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'jlake_DB'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor = cnx.cursor()

#Load json file
inputFile = open('restaurantData.json','r')
restaurantDict = json.load(inputFile)
inputFile.close()


#Loop through the restaurants and add info and menu to database
for key, restaurant in restaurantDict.iteritems():
	
	###############################
	## Add restaurant info first ##
	###############################

	inputDict = {
		'restId' : key,
		'name' : restaurant['name'],
		'address' : restaurant['street_address'],
		'city' : restaurant['locality'],
		'state' : restaurant['region'],
		'zip' : restaurant['postal_code'],
		'phone' : restaurant['phone'],
		'lat' : restaurant['lat'],
		'lng' : restaurant['long'],
		'url' : restaurant['website_url']
	}

	#Insert this info into the database
	addRestaurant = ("INSERT INTO restaurants (restId, name, address, city, state, zip, phone, lat, lng, url) VALUES (%(restId)s,  %(name)s, %(address)s, %(city)s, %(state)s, %(zip)s, %(phone)s, %(lat)s, %(lng)s, %(url)s)")

	#Give cursor strings to submit a query
	cursor.execute(addRestaurant,inputDict)





#Get menu data: 
for key,restaurant in restaurantDict.iteritems():

	i=0
	#Loop through all of the menus in res: 
	for m in restaurant['menus']:
		i = i+1
		hasher = [i,key,restaurant['name'],i+2,m['menu_name']]
		menID = abs(hash(str(hasher))) % (10 ** 20)
		foods = []
		inputDict = {
			'restId':key,
			'menuId':menID,
			'restName':restaurant['name'],
			'menuName': m['menu_name']
		}
		
		addMenu = ("INSERT INTO menus (restId, menuId, restName,  menuName) VALUES (%(restId)s , %(menuId)s, %(restName)s  ,%(menuName)s)")
		cursor.execute(addMenu,inputDict);

		#Now we need to populate the menuItems table:
		for s in m['sections']:
			for ss in s['subsections']:
				for item in ss['contents']:
					if item['type'] == "ITEM":
						i=i+1
						hasher = [i,s,ss,i+45,item['name']]
						itID =abs(hash(str(hasher))) % (10 ** 20)
						itemDict = {
							'menuId': menID,
							'itemId': itID,
							'sect': s['section_name'].encode('utf-8'),
							'type': item['type'].encode('utf-8'),
							'name': item['name'].encode('utf-8'),
							'menuName': m['menu_name']
						}
						if item['name'] not in foods:
							foods.append(item['name'])
					
							if item.has_key('description'):	
								itemDict['des'] = item['description']
								if item.has_key('price'):
									itemDict['price'] = item['price']
									addItem = ("INSERT INTO menuItems (menuId, itemId, des, menuName, sect, name, price ) VALUES (%(menuId)s, %(itemId)s, %(des)s, %(menuName)s, %(sect)s, %(name)s, %(price)s)")
								else:

									addItem = ("INSERT INTO menuItems (menuId, itemId, menuName, sect, name ) VALUES (%(menuId)s, %(itemId)s, %(menuName)s, %(sect)s, %(name)s)")

							else:
								if item.has_key('price'):
									itemDict['price'] = item['price']
									addItem = ("INSERT INTO menuItems (menuId, itemId, menuName, sect, name, price ) VALUES (%(menuId)s, %(itemId)s, %(menuName)s, %(sect)s, %(name)s, %(price)s)")
								else:
									addItem = ("INSERT INTO menuItems (menuId, itemId,menuName, sect, name) VALUES (%(menuId)s, %(itemId)s, %(menuName)s, %(sect)s, %(name)s)")


		
							cursor.execute(addItem,itemDict);









cnx.commit()
cnx.close()

