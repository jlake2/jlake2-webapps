#John F. Lake, Jr. 
#Altered Restaurant ID File
''' Controller for /restaurants/{id}
    Imported from handler for /restaurants '''
import cherrypy
import mysql.connector
from menus import Categories
class RestaurantID(object):
    ''' Handles resource /restaurants/{id} 
        Allowed methods: GET, PUT, DELETE, OPTIONS '''
    exposed = True

    def __init__(self):
        self.menus=Categories()

    def GET(self, restId):
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='jlake_DB',charset='utf8mb4')        
	cursor = cnx.cursor()
	q = "select * from restaurants where restId='%s';" % restId
	cursor.execute(q)
	info = list(cursor.fetchall()[0])
	result = "<!DOCTYPE=html><html><body>"
	for item in info:
		result = result + str(item) + "<br>"
	result = result + "</body><html>"
	return result
	
        

    def PUT(self, restId, **kwargs):
        ''' Update restaurant with restId'''
        result = "You called a PUT command on /restaurants/{restId=%s}.  This isn't currently implemented.\n" % restId
        result += "PUT message body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update restaurant
        # Prepare response
        return result

    def DELETE(self, restId):
        ''' Delete restaurant with restId'''
        #Validate restId
        #Delete restaurant
        #Prepare response
        return "You called DELETE on /restaurants/{id=%s}\n Not currently implemented. " % restId

    def OPTIONS(self, restId):
        ''' Allows GET, PUT, DELETE, OPTIONS '''
        #Prepare response
        return "<p>/restaurants/{id} allows GET, PUT, DELETE, and OPTIONS</p>"
