#John Lake
#menus.py - handles /restaurants/{ID}/menus

import os, os.path, json, logging, mysql.connector
import cherrypy
from jinja2 import Environment, FileSystemLoader
from menuid import CategoryID
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))




class Categories(object):
    exposed = True

    def __init__(self):
        self.id = CategoryID()
        self.db=dict()
        self.db['name']='jlake_DB'
        self.db['user']='root'
        self.db['host']='127.0.0.1'


 
    def getDataFromDB(self,id):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()
	q="select name from restaurants where restId='%s' " % id
	cursor.execute(q)
	restName = cursor.fetchone()
        q="select menuId ,menuName from menus where restId='%s' " % id
        cursor.execute(q)
        result=cursor.fetchall()
        return restName,result

    def GET(self, restId):
        ''' Return list of menus for restaurant restId'''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            restName,result=self.getDataFromDB(restId)
        except mysql.connector.Error as e:
            logging.error(e)
            raise

        if output_format == 'text/html':
            return env.get_template('menus-tmpl.html').render(
                rId=restId,
                rName=restName,
                menus=result,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            data = [{
                'href': 'restaurants/%s/menus/%s/items' % (restId, menuId),
                'name': menuName
            } for menuId, menuName in result]
            return json.dumps(data, encoding='utf-8')

    def POST(self, **kwargs):
        result= "You called POST on /restaurants/{restId}/menus.  This isn't implemented yet."
        result+= "Message body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self,restId):
        return "<p>/restaurants/{restId}/menus/ allows GET, POST, and OPTIONS</p>"
