#John F. Lake, Jr. 
#Handler for /restaurants/{restId}/menus/{menuId}/items/{itemId}

import cherrypy
import mysql.connector
class ItemID(object):
    exposed = True

    def GET(self, restId, menuId, itemId):

        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='jlake_DB',charset='utf8mb4')        
	cursor = cnx.cursor()
	q = "select * from menuItems where menuId='%s';" % menuId
	cursor.execute(q)
	info = list(cursor.fetchall()[0])
	result = "<!DOCTYPE=html><html><body>"
	for item in info:
		if item is not None:
			if type(item) is float:
				result = result + str(item) + "<br>"
			else:
				result = result + item.encode('utf-8') + "<br>"
	result = result + "</body><html>"
	return result  

    def PUT(self, restId, menuId, itemId, **kwargs):
        result = "You called PUT on /restaurants/{restId}/menus/{menuId}/items/{itemId}\n"
	result += "Message body: \n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update restaurant
        # Prepare response
        return result

    def DELETE(self, restId, menuId, itemId):
        #Validate id
        #Delete restaurant
        #Prepare response
        return "You called DELETE on /restaurants{restId}/menus/{menuId}/items/{itemId} \n"
    def OPTIONS(self, restId, menuId, itemId):
        #Prepare response
        return "<p>/restaurants{restId}/menus/{menuId}/items/{itemId} allows GET, PUT, DELETE, and OPTIONS</p>"

