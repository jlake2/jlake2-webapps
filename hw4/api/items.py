#John F. Lake, Jr. 
#Handler for /restaurants/{restId}/menus/{menuId}/items
''' Implements handler for /items
Imported from handler for /menus/{id} '''
import logging
import cherrypy
import mysql.connector
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import os
import os.path
import json
import pprint
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
from itemid import ItemID

class Items(object):
    ''' Handles resources /restaurants{restID}/menus/{catID}/items}
        Allowed methods: GET, POST, OPTIONS  '''
    exposed = True

    def __init__(self):
        self.id = ItemID()
        self.db=dict()
        self.db['name']='jlake_DB'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self,menuId):
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'])
        cursor = cnx.cursor(dictionary=True)
        q="select menuName, itemId, name, des, price from menuItems where menuId=%s" % menuId
        cursor.execute(q)
        result=cursor.fetchall()
	for item in result:
		if item['price'] is None:
			item['price'] = "Call or ask for price."
		if item['des'] is None:
			item['des'] = "No description."
        return result

    def GET(self, restId, menuId):
        ''' Return list of items for menus for restaurant id'''
        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        result=self.getDataFromDB(menuId)
        
        if output_format == 'text/html':
            return env.get_template('items-tmpl.html').render(rID=restId,cID=menuId,cName=result[0]['menuName'],items=result,base=cherrypy.request.base.rstrip('/') + '/')
        else:
            data = [{
                    'href': '/restaurants/%s/menus/%s/items/%s' % (restId, menuId, item['itemId']),
                    'name': item['name'],
                    'description': item['des'],
                    'price' : unicode(item['price'])
                    } for item in result]
            return json.dumps(data, encoding='utf-8')
        
    def POST(self, restId, menuId,  **kwargs):
        result= "You called POST on /restaurants/{restId}/menus/{menuId}/items\n"
        result+= "Message body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self,restId, menuId):
        return "<p>/restaurants/{restId}/menus/{menuId}/items allows GET, POST, and OPTIONS</p>"

