#John F. Lake, Jr. 
#menuid.py - handles /restaurants/{ID}/menus/{menuID}

import cherrypy
import mysql.connector
from items import Items
class CategoryID(object):
    exposed = True

    def __init__(self):
        self.items = Items()   #Points to items.py

    def GET(self, restId, menuId):
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='jlake_DB',charset='utf8mb4')        
	cursor = cnx.cursor()
	q = "select * from menus where menuId='%s';" % menuId
	cursor.execute(q)
	info = list(cursor.fetchall()[0])
	result = "<!DOCTYPE=html><html><body>"
	for item in info:
		if item is not None:
			if item is info[0]:
				result = result + "restId:  " + item.encode('utf-8')+ "<br>"
			elif item is info[1]:
				result = result + "menuId:  " + item.encode('utf-8')+ "<br>"
			else:
				if type(item) is float:
					result = result + str(item) + "<br>"
				else:
					result = result + item.encode('utf-8') + "<br>"
	result = result + "</body><html>"
	return result  

    def PUT(self, restId, menuId, **kwargs):
        ''' Update menu id for restaurant id'''
        result = "You called PUT for /restaurants/{restId}/menus/{menuId}  Not currently implemented "
        result += "Message body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update restaurant
        # Prepare response
        return result

    def DELETE(self, restId, menuId):
        #Validate id
        #Delete restaurant
        #Prepare response
        return "You called DELETE for /restaurants/{restId}/menus/{menuId}  Not currently implemented "
	

    def OPTIONS(self,restId, menuId):
        return "<p>/restaurants/{restId}/menus/{menuId} allows GET, PUT, DELETE, and OPTIONS</p>"

