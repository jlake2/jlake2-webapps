''' Implements handler for /orders/{orderId}/items/{itemId}
To add an item to an order '''
import apiutil
import cherrypy
import mysql.connector
from mysql.connector import Error
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import json
from apiutil import errorJSON,  SESSION_KEY
from config import conf

class OrderItemsID(object):
    exposed = True
    @cherrypy.tools.json_in()

    
    def GET(self, orderId, itemId):
        ''' GET orderItemId,quantity for orderId and itemId or error if no entry with orderId or itemId'''
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='jlake_DB',charset='utf8mb4')        
        cursor = cnx.cursor()
        q="select exists(select 1 from orderItems where orderId=%s and itemId=%s)" % (orderId, itemId)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #no orderItemId for this orderId / itemId combination
            return errorJSON(code=9004, message="OrderItemId for OrderId %s  and ItemId %s Does Not Exist" % (orderId,itemId))
        q="select orderItemId,quantity from orderItems where orderId=%s and itemId=%s" % (orderId, itemId)
        cursor.execute(q)
        tmp=cursor.fetchall()
        result={"orderItemId":tmp[0][0],"quantity":tmp[0][1],"errors":[]}
        return json.dumps(result)


    def DELETE(self, orderId, itemId):
        ''' Delete orderItem with orderId and itemId'''
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='FeedND',charset='utf8mb4')        
        cursor = cnx.cursor()
        q="set sql_safe_updates=0;"  
        cursor.execute(q)
        q="select exists(select 1 from orderItems where orderId=%s and itemId=%s)" % (orderId, itemId)
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #no orderItemId for this orderId / itemId combination
            return errorJSON(code=9004, message="OrderItemId for OrderId %s  and ItemId %s Does Not Exist" % (orderId,itemId))
        try:
            q="delete from orderItems where orderId=%s and itemId=%s" % (orderId, itemId)
            cursor.execute(q)
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert orderItem
            print "mysql error: %s" % e
            return errorJSON(code=9005, message="Failed to delete order item from shopping cart")
        result={"errors":[]}
        return json.dumps(result)


    @cherrypy.tools.json_in(force=False)
    def PUT(self, orderId, itemId):
        ''' Add or update an item to an order 
        quantity is received in a JSON dictionary
        output is also returned in a JSON dictionary'''
        try:
		data = cherrypy.request.json
		quantity = data['quantity']
		print "quantity received: %s" % quantity
        except:
	    print "quantity was not received"
            return errorJSON(code=9003, message="Expected integer 'quantity' of items in order as JSON input")
        sess = cherrypy.session
        username = sess.get(SESSION_KEY, None)
        #print "order items SESSION KEY %s" % SESSION_KEY
        #print "value of SESSION KEY %s" % cherrypy.session[SESSION_KEY]
        if username:
            print "found username %s of session %s" % (username, SESSION_KEY)

        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='jlake_DB',charset='utf8mb4')        
        cursor = cnx.cursor()
        q="set sql_safe_updates=0;"  
        cursor.execute(q)
        # does orderID exist?
        q="select exists(select 1 from orders where orderId=%s)" % orderId
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #orderID does not exist
	    q = "insert into orders (nameFirst, nameLast) values (\"John\", \"Lake\");"
	    cursor.execute(q)
            #return errorJSON(code=9000, message="Order with OrderId %s Does Not Exist") % orderId
        # does itemID exist?
        q="select exists(select 1 from menuItems where itemId=%s)" % itemId
        cursor.execute(q)
        if not cursor.fetchall()[0][0]:
            #itemID does not exist
            return errorJSON(code=9001, message="Item with ItemID %s Does Not Exist") % itemId
	#Get the name of the item: 
	q = "select name from menuItems where itemId=%s;" % itemId
	cursor.execute(q)
	n = cursor.fetchall()[0][0]


        q="insert into orderItems (orderId, itemId, itemName, numItems) values (%s, %s, '%s', %s) on duplicate key update numItems=%s;" \
            % (orderId, itemId, n, quantity, quantity)
        cursor.execute(q)
        q="select orderItemId from orderItems where orderId=%s and itemId=%s;" % (orderId,itemId)
        orderItemID=0
        #try:
    	cursor.execute(q)
    	orderItemID=cursor.fetchall()[0][0]
    	cnx.commit()
    	cnx.close()
        #except Error as e:
            #Failed to insert orderItem
        #    print "mysql error: %s" % e
        #    return errorJSON(code=9002, message="Failed to add order item to shopping cart")
        result = {'orderItemID':orderItemID, 'orderID':orderId, 'itemID':itemId, 'quantity':quantity, 'errors':[]}
        return json.dumps(result)

application = cherrypy.Application(OrderItemsID(), None, conf)

