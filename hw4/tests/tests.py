import requests
s = requests.Session()
host='http://ec2-52-0-37-90.compute-1.amazonaws.com/'
s.headers.update({'Accept': 'application/json'})


#Restaurants
r = s.get(host+'/restaurants',)
print r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.post(host+'/restaurants')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)

r = s.get(host+'/restaurants/a2c367a73b6181fec216')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)

r = s.get(host+'/restaurants/a2c367a73b6181fec216/menus')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.get(host+'/restaurants/a2c367a73b6181fec216/menus/5119023949780098380')
print '\n', r.status_code, r.text

r = s.get(host+'/restaurants/a2c367a73b6181fec216/menus/5119023949780098380/items')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

#Orders
r = s.get(host+'/orders',)
print r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)

r = s.post(host+'/orders')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)


r = s.options(host+'/orders')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)



r = s.get(host+'/orders/1/items')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)

r = s.put(host+'/orders/1/items')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)

r = s.delete(host+'/orders/1/items')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)
r = s.options(host+'/orders/1/items')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)

