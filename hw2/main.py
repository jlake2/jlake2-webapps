import collections
import cherrypy
import sys
import mysql.connector

class ExampleApp(object):
    @cherrypy.expose
    def index(self):
	rests = {}
        cnx = mysql.connector.connect(user='root',
                              host='127.0.0.1',
                              database='jlake_DB')
        cursor = cnx.cursor()
        query = ("SELECT name,restId from restaurants")
        cursor.execute(query)
        info = "<ul>"
        for (name,restId) in cursor: 

		info += "<li><h1><a href=\"http://ec2-52-0-37-90.compute-1.amazonaws.com/api/showdb/?rest=%s\">" % restId
		info += "%s </a></h1></li>" % name
	info +="</ul>"


	return '''
		<!DOCTYPE html>
		<html>
			<head>
				<title>FEEDND-Find great restaurants around Notre Dame!</title>
				<h1>FEEDND- Find restaurants that are on and around Notre Dame!<h1>
			</head>
			<body>
				<a href=\"http://www.google.com\"> Orders</a>  
				<a href=\"http://www.google.com\"> Account</a>
				<p>Restaurants Menus:</p>''' +info +'''	


				<p>by John Lake (jlake2)</p>
			
			</body>
		</html>
		'''


    @cherrypy.expose
    def showdb(self,rest):
	sections = {}
	#Rests will hold all of the information we need. 
        cnx = mysql.connector.connect(user='root',
                              host='127.0.0.1',
                              database='jlake_DB')
        cursor = cnx.cursor(dictionary=True)

        info = "" #The string we will ultimately return
	rName = ""
	disp = ""


	query = ("SELECT restName from menuItems WHERE restId ='%s'") % rest
	cursor.execute(query)
	for row in cursor:
		rName = row['restName']

	info += "<h1>%s</h1><br>" % rName
	info += "<ul>"


	query = ("SELECT * from hours WHERE restId = '%s'") %rest
	cursor.execute(query)
	for row in cursor: 
		if row['day'] == 'M':
			info +="<li>Monday: %s to %s </li>" % (row['open'],row['close'])
		elif row['day'] == 'T':
			info +="<li>Tuesday: %s to %s </li>" % (row['open'],row['close'])
		elif row['day'] == 'W':
			info +="<li>Wednesday: %s to %s </li>" % (row['open'],row['close'])
		elif row['day'] == 'TH':
			info +="<li>Thursday: %s to %s </li>" % (row['open'],row['close'])
		elif row['day'] == 'F':
			info +="<li>Friday: %s to %s </li>" % (row['open'],row['close'])
		elif row['day'] == 'S':
			info +="<li>Saturday: %s to %s </li>" % (row['open'],row['close'])
		elif row['day'] == 'SU':
			info +="<li>Sunday: %s to %s </li>" % (row['open'],row['close'])
	info += "</ul>"

	query = ("SELECT sect from menuItems WHERE restId ='%s'") % rest
	cursor.execute(query)
	for row in cursor: 
		if row['sect'] not in sections:
			sections[row['sect']] = {}
			sections[row['sect']]['name'] = row['sect']
			sections[row['sect']]['items'] = []



	query = ("SELECT sect, name from menuItems WHERE restId ='%s'") % rest
	cursor.execute(query)
	for row in cursor: 
		sections[row['sect']]['items'].append(row['name']);

	info += "<ul>"
	for key in sections:	
		info += "<h2>%s</h2><br>" % sections[key]['name']
		for item in sections[key]['items']:
			info += "<li>%s </li>" % item
	info += "</ul>"
	
	return info
		
application = cherrypy.Application(ExampleApp(), None)
