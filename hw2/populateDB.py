#Use this script to populate the |restaurant| table of the database

import mysql.connector
import json
from decimal import *
import random

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'jlake_DB'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor = cnx.cursor()

#Load json file
inputFile = open('restaurantData.json','r')
restaurantDict = json.load(inputFile)
inputFile.close()


#Loop through the restaurants and add info and menu to database
for key, restaurant in restaurantDict.iteritems():
	
	###############################
	## Add restaurant info first ##
	###############################

	inputDict = {
		'restId' : key,
		'name' : restaurant['name'],
		'address' : restaurant['street_address'],
		'city' : restaurant['locality'],
		'state' : restaurant['region'],
		'zip' : restaurant['postal_code'],
		'phone' : restaurant['phone'],
		'lat' : restaurant['lat'],
		'lng' : restaurant['long'],
		'url' : restaurant['website_url']
	}

	#Insert this info into the database
	addRestaurant = ("INSERT INTO restaurants (restId, name, address, city, state, zip, phone, lat, lng, url) VALUES (%(restId)s,  %(name)s, %(address)s, %(city)s, %(state)s, %(zip)s, %(phone)s, %(lat)s, %(lng)s, %(url)s)")

	#Give cursor strings to submit a query
	cursor.execute(addRestaurant,inputDict)





#Get menu data: 
for key,restaurant in restaurantDict.iteritems():

	#Loop through all of the menus in res: 
	for m in restaurant['menus']:
		foods = []
		inputDict = {
			'restId':key,
			'restName':restaurant['name'],
			'menuName': m['menu_name']
		}

		addMenu = ("INSERT INTO menus (restId, restName,  menuName) VALUES (%(restId)s , %(restName)s  ,%(menuName)s)")
		cursor.execute(addMenu,inputDict);

		#Now we need to populate the menuItems table:
		for s in m['sections']:
			for ss in s['subsections']:
				for item in ss['contents']:
					if(item['type'] == "ITEM"):
						itemDict = {
							'restId': key,
							'restName': restaurant['name'],
							'menuName': m['menu_name'], 
							'sect': s['section_name'],
							'type': item['type'],
							'name': item['name'],
						}
						if(itemDict['name'] not in foods):
							foods.append(itemDict['name'])
						
							if(item.has_key('price')):
								itemDict['price'] = item['price']
								addItem = ("INSERT INTO menuItems (restId, restName, menuName, sect, name, price ) VALUES (%(restId)s , %(restName)s, %(menuName)s, %(sect)s, %(name)s, %(price)s) ")
							else:
								addItem = ("INSERT INTO menuItems (restId, restName, menuName, sect, name) VALUES (%(restId)s , %(restName)s, %(menuName)s, %(sect)s, %(name)s)")
							cursor.execute(addItem,itemDict);









cnx.commit()
cnx.close()

